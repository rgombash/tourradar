### To start the stack

```
docker-compose up --scale apache2=2
```

This will start with 2 apache servers, it can be changed to any sane number of web servers

### Links

Main http entry point

http://127.0.0.1

HAProxy stats:

http://127.0.0.1:1936 (stats:stats)

Kibana:

http://127.0.0.1:5601

Needs initial index pattern setup via GUi : Management -> Index Pattern : * -> Time filed : @timestamp -> Create index.

Jenkins:

http://127.0.0.1:8080 (admin:admin)
