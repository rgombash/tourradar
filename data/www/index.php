<?php
session_start();

$count = isset($_SESSION['count']) ? $_SESSION['count'] : 1;
$_SESSION['count'] = ++$count;
$data['count'] = $_SESSION['count'];

$data['server_addr'] = $_SERVER['SERVER_ADDR'];
$data['date'] = date("Y/m/d");

//$data['ip'] = $_SERVER['HTTP_CLIENT_IP'];
$data['clinet_ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
//$data['ip'] = $_SERVER['REMOTE_ADDR'];

echo json_encode($data);